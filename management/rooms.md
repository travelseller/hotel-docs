Roomcategorys
-------------

Shorthand for `Sales => Product => show products`

The `Room category`-Producttype is simply a Producttype whos quantity is calculated by the amount of days between a given Checkin- and Checkoutdate.

### create a Room category

1. From Propertymanagement: Choose `Roomcategorys => create`
1.1 From Elsewhere: Choose `Sales => Product => show products => create => Type Room category`

In the [Inputscreen](../basics/inputscreen.md) provide these Informations:

3. check `can be sold`
4. title: type the Title of the Room category (e.g.: "Sunny Suite")
5. Room code: the Roomcode (e.g.: "DBL") (aka SKU). The System validates the uniquness of this value
6. baseprice: The [Baseprice](../basics/terms.md#baseprice)
7. Unit of measure (UOM): select "Day"
8. Allotments: [create Allotments](createallotments.md) for this Roomcategory
9. assigned Addons: assign possible Addons by check the checkbox for each allowed Addon
10. assigned Rates: assign possible Rates by checking the checkboy for each allowed Pricelist

### update a Room category

1. From Propertymanagement: Choose `Roomcategorys`
1.1 From Elsewhere: Choose `Sales => Product => show products`
1.2 On the [Gridscreen](../basics/gridscreen.md), set the `data type`-filter to `Room categorys`
1.3 Add further Filters and locate the Room category you want to update
2. Choose the Edit-Option within the [Actiongroup](../basics/gridscreen.md#actiongroup)
3. On the Formularscreen update the desired informations.
4. choose `save` to update the Room category