Pricing
-------

Shorthand for `Sales => Pricing => show all`


### Introduction

products (e.g. Roomcategorys or Addons) can be assigned to a pricelist. 
A pricelist is a Container for Calculation-Rules and optional Date-Constraints.

A Price ([Baseprice](../basics/terms.md#baseprice)) given to Pricelist will be calculated by passing each Calculation-Rule and becomes a [conditioned price](../basics/terms.md#conditionedprice).

e.g.: 
```
Lets asume, the Roomcategory "DBL Room" has a Baseprice "100,00 EUR" and the Pricelist "Season A" configured.
We want to calculate the price for 3 PAX (1 PAX is 5 years old).

Each Calculation-Rule will be applied to the Baseprice (100,00 EUR):

initial: price = 100 EUR (Baseprice is the initial price)

	||
	\/

Rule 1: add 10% to price (100 EUR)
price = 110 EUR

	||
	\/

Rule 2: add 5 EUR to price (110 EUR) for each PAX if the amount of PAX > 2
price = 125 EUR

	||
	\/

Rule 3: discount 10% if at least one PAX's age is lesser than 6
price = 112,50 EUR

112,50 EUR is the amount to pay for each Day.
```

```
Pricelist "Saison A"
|-- Date Constraints
	|-- 01.07.2020 till 18.07.2020
	|-- 01.08.2020 till 15.08.2020
	|-- 01.09.2020 till 13.09.2020
|-- Calculation Rules
	|-- Rule 1 "add 10% to the Baseprice"
	|-- Rule 2 "add 5 EUR to the Price for each PAX if the amount of PAX > 2"
	|-- Rule 3 "discount 15% for each PAX whos age is lesser than 6"
```


### Calculation-Rules

The Product's Baseprice will be assigned to the first rule (Rule 1) of the Pricelist. If a rule has a "condition_expression" defined then the rule will only be applied to the price if the condition is true.

```
Pricelist
|-- Rule 1
	|-- title: "add 10% to the Baseprice"
	|-- condition_expression: not defined (PriceItem will be evaluated)
	|-- price_expression: "price*1.1"
|-- Rule 2
	|-- title: "add 5 EUR to the Price for each PAX if the amount of PAX > 3"
	|-- condition_expression: "pax.total() > 3"
	|-- price_expression: "price+pax.total*5"
|-- Rule 3
	|-- title: "grant 10% discount for each Child-PAX"
	|-- condition_expression: "pax.ages(0,17) > 0"
	|-- price_expression: "price-(price*0.1)*pax.ages(0,17)"
```

Note: the expression "pax.ages(0,17)" returns the number of PAX age >= 0 AND <= 17

Rule 1 has no condition_expression (null) and will be evaluated.
Rule 2 will be evaluated, if the PAX count is greater than 3
Rule 3 will be evaluated, if the count if PAX ages >= 0 AND <= 17

These expression are currently available:

### scalars

- `price`: The current calculated price.
- `baseprice`: shorthand for `product.baseprice`
- `product.sku`: product's sku
- `product.baseprice`: product's baseprice
- `timerange_from`: CheckIn date in Format yyyy-mm-dd
- `timerange_till`: CheckOut date in Format yyyy-mm-dd
- `partner.id`: The Contract-Partner's System ID.
- `partner.is_company`: Whether the partner is a company (B2B)
- `partner.customer`: Whether the partner is customer (B2C)
- `partner.supplier`: Whether the partner is your supplier (Purchase)
- `partner.zip`: Partners ZIP-code
- `partner.email` Partner Email
- `partner.city` Partner City
- `partner.country` Partner Country
- `partner.scope` Partner Scope ID

### functions

- `pax.ages(from,till)`: the total number of paxes in given Age Range

e.g.: 

`pax.ages(0,6)`: will be populated with the count of Pax whos age > 0 and < 6

`pax.ages(17,100)`: will be populated with count of Pax ages > 18 and < 100


- `pax.total()`: all Sum all pax
