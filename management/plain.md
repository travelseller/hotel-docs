Plain Sections Masterdata and NBC/Content
-----------------------------------------

The Masterdata and NBC/Content are by default empty: a blank page will be shown.
In order to fill these Modules with Inputfields you have to customize these Modules with the [Customizer](../basics/eav.md).