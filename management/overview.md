Propertymanagement
------------------

this Module acts as Controlpanel for Hotelmanagers, all the Hotel-Functionalitys are grouped by this Module.
As Admin-User, you can access each Module besided the Propertymanager through the corresponding native Module:

e.g.: `Propertymanagement => Roomcategorys` is a shorthand for `Sales` => `Product` => `show products` (filtered by data type `Room category`)

To switch to a specific Property (Hotel), you can use the Shorthand `Propertys` Dropdown in the Primary Navigation.

![Screenshot of Propertys Dropdown Screen](../img/propertysdropdown.png)


functionalitys:

- [Masterdata](plain.md) (must be customized)
- [NBC/Content](plain.md) (must be customized)
- [Pricing](pricing.md)
- [Roomcategorys](rooms.md) (Room masterdata,Allotments)
- [Systemuser](systemuser.md)
- [Customer/Guests](customer.md)
- [Sales/Bookings](sales.md)