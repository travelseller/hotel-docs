Gridscreen
----------

The Gridscreen lists all Data Models for the currently active Module in term of a Datagrid. The Datagrid provides Filters to filter, search and sort through the Results.

![Screenshot of a Gridscreen](../img/gridscreen.png)

1. Column Selector: show/hide Grid Columns
2. Export: Exports the Grid in various Formats
3. Pagination: select the number of Rows per site
4. Columns: click on the columnname to enable sorting
5. Filters
6. Actiongroup: possible Actions for each Row in the Grid
7. Create Button: create new Data

### <a name="actiongroup"></a>Actiongroup

the Actiongroup is a Column of the Datagrid which provides Actions for a particular Datarow.
The most common actions are:

- view data ![icon for view action](../img/viewaction.png)
- update data ![icon for update action](../img/updateaction.png)
- delete data ![icon for delete action](../img/deleteaction.png)
