Homescreen
----------

The homescreen displays all available Modules in term of a Icongrid.
You can access the homescreen by choosing the homescreen-icon on the upper left corner.
By clicking an Icon the corresponding Module will be started.

![Homescreen Icon](../img/homescreenicon.png)

![Homescreen](../img/homescreen.png)