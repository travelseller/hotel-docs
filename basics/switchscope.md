### Switch Scope

1. Choose [Clients & Users](../users/index.md) => `Clients` => `show all`
2. Add further Filters and locate the Client-Scope you want to switch to
3. Choose the Changebutton ![Image of the Changebutton](../img/changebutton.png) within the [Actiongroup](../basics/terms.md#actiongroup)

### Return to origin Scope

1. click the `release scope` Button in the Main Navigation.
![Image of the Release-Scope-Button](../img/switchscope.png)

### Result

You have switched to the Client-Scope. All your settings and actions belonging now to this Client.
The Top-Navigation displays your currently active besides the `release scope` Button.
The later will return you to your origin scope.