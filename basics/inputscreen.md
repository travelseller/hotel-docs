Inputscreen
-----------

The Inputscreen collects and validates all the Data that is needed to modify a Datamodel.

![Screenshot of a Inputscreen](../img/inputscreen.png)

1. Lables
2. Inputfields (Dropdown, Datepickers, Textfields ...)
3. Tabs with additional informations
4. Save Button: saves the Data
5. Cancel Button: cancel the transaction anf returns to the [Gridscreen](gridscreen.md)	