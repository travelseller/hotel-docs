Customizer
----------

tshotel provides the ability to define custom Inputfields in nearby all Programmmodules.
Lets mind this simple example to introduce the Customizer functionalities:

The Systemowner (you or your Serviceprovider) has defined a Preset of Inputfields for the NBC (Non-Booking-Content)-Module: 

- `Adress` as Textinput
- `Pictures` as Fileinput for images
- `Geo-Position` as Geopicker

The Hotel "Sacred Heart" calls in and asks for a additional Inputfield:

- `Distance to beach` as Textinput

The Customizer makes it possible for you to provide these addionaly fields for the Hotel.

### Here's how you do it:

1. [Switch](switchscope.md) to the client you want to manage
2. Head to the [System Settings](systemsettings.md)
3. Choose `customizer` in the Navigation on the left side
4. In the Dropdownfield, choose the Datamodel `Content (NBC)`
The Inputscreen displays all existing Inputfields for the selected Datamodel (if any). You can view,edit or delete these Inputfields.
In order to add another Inputfield:
6. Choose `new item`: a Popup-Window appears
7. On the Popup's Formularscreen Enter the following information:

- State: active
- Inputtype: choose `Inputfield`
- Description: enter `Distance to beach`

8. choose `save`

### Result

a new Inputfield for the Datamodel `Content (NBC)` is created.
To check the new Inputfield, head to the [Propertymanagement](../propertys/overview.md) => NBC/Content