Systemsettings
--------------

The tshotel core and most other Programmmodules provides Systemsetting in order to let you configure the behavior of the Application component.
Choose the cogwheel icon in the upper right corner, besided your Username ![image of cogwheel icon](../img/cogwheel.png).

The Navigation on the left shows all Programmmodules providing Setting abilities.
A click on the Programmmodule opens the [Inputscreen](basics/inputscreen.md) for Setup the Module.

Refer to the Programmmodules Documentation to learn how to configure the module.