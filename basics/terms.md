Terms
-----

### <a name="dataselectionscreen"></a>Datatypeselection Screen

Most Datamodels are grouped by a Metamodel, e.g.: "Default Product" and "Room category" share the same Metamodel "Product".
If you want to create Data, you have to select the concrete Model Type.

![Screenshot of a Dataselection Screen](../img/modeltypeselection.png)

###  <a name="baseprice"></a>Baseprice

The Baseprice is the initial price for Pricecalculations.
Calculationrules modifys the Baseprice and calculates the final price per unit (Conditioned Price).

### <a name="conditionedprice"></a>Conditioned Price

The result of Pricecalculations.

