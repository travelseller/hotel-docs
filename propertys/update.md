update a existing Property
-------------------------

1. Choose [Clients & Users](../users/index.md) => `Clients` => `show all`
2. On the [Gridscreen](../basics/gridscreen.md), set the `data type`-filter to `propertys`
3. Add further Filters and locate the property you want to update
4. Choose the Edit-Option within the [Actiongroup](../basics/gridscreen.md#actiongroup)
5. On the Formularscreen update the desired informations.
6. choose `save` to update the Hotel-Client.

### Result

The Client was updated with the entered informations.