Propertys (Hotels)
------------------

In tshotel, a hotel is defined as a client which acts as Scope for a logged in Systemuser, henceforth we will use the term `scope` instead of `client`. Each Scope can obtain several Entitys such like Roomcategorys, Room-Rates, Non-Booking-Content or Bookings. All those Entitys are only accessable by the Scope itself and the Parent-Scopes (figure 1: `Entity Tree`).
A Scope is a Unit for Data- and Accesscontroll (Organisationunit).


```
Figure 1: Entity Tree

Kempinski Hotels S.A.
	|-- User: ceo@kempinski.de
	|-- Hotel: "Hotel Adlon Berlin"
		|-- User: manager.markus@kempinski.de
		|-- NBC: Editing and Pictures
		|-- Roomcategory: "Junior-Suit"
			|-- Allotments
			|-- Rates (Pricing)
		|-- Roomcategory: "Standard"
			|-- Allotments
			|-- Rates (Pricing)
		|-- Bookings
		|-- Guests (Customers)
		|-- Systemuser
		|-- REST/SOAP API
	|-- Hotel "Hotel Vier Jahreszeiten"
		|-- User manager.mascha@kempinski.de
		|-- NBC: Editing and Pictures
		|-- Roomcategorys
			|-- Allotments
			|-- Rates (Pricing)
```

In the Entity-Tree above, the user ceo@kempinski.de has access to his own Scope (Kempinski Hotels S.A.) and to all subsidiary Scopes ("Hotel Adlon Berlin" and "Hotel Vier Jahreszeiten").

The Systemusers manager.markus@kempinski.de and manager.mascha@kempinski.de have only access to their own Scopes and dont have access to the parentscope (Kempinski Hotels S.A.).

Each Scope can obtain multiple Systemusers with individual Accessrights.