Create a new Property
---------------------

1. Choose [Clients & Users](../users/index.md) => `Clients` => `show all` => `create client`
2. On the Datatypeselection-Screen choose `propertys` and set a title, finaly choose `save`
3. On the Formularscreen Enter the following information:

- Client: The parent client as Owner of this client (if this Hotel a Member of a Hotelchain, you can assign this property to the chain here)
- state: choose `active`
- Title: The Title of your Property
- available Apps: choose at least `Propertymanagement`, `Clients & Users` and `Products`
- misc: optionaly upload a Logo

4. choose `save` to create the Hotel-Client.

### Result

The Client is created. The Datatype of the Client is `propertys` and provides therfore all neccessary Functions needed by an Property.

### Next Steps

- customize the newly created Property with the EAV-System
- add Systemusers to the Propertys
- let the Systemusers manage NBC, Rooms and Allotments