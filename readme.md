Travelseller Hotel
------------------

Travelseller Hotel (tshotel) is a Multitenancy B2B Business-Application suited for Hotel-Resource-Management.
Tshotel provides a interface for common Tasks of property marketing:

- [create Propertys](propertys/create.md) and [manage them](propertys/update.md)
- manage Hotelchains: Propertys can obtain infinite Child-Propertys
- create individual inputfields for each Property
- create NBC (non-booking-content) for your propertys in multiple languages
- manage Rates with a powerfull pricingsystem
- manage Addons
- manage Roomcategorys and assign Addons and Rates
- create or suspense Allotments for each Roomcategory
- Manage Contracts with external Fulfiller (allotment-fulfillment)


General Useage
--------------

### Start Apps

The Graphical Userinterface provides a [Homescreen](basics/homescreen.md) which list all accessable Apps for the current logged in User. To Start an App, open the [Homescreen](basics/homescreen.md) and choose the desired app.

### Screens

each Tshotel Programmmodule provides at least two Screens: the [Gridscreen](basics/gridscreen.md) and the [Inputscreen](basics/inputscreen.md).
The [Gridscreen](basics/gridscreen.md) lists and filters Data and provide an [Actiongroup](basics/actiongroup.md) with possible Actions (e.g.: view, edit, delete) for a particular Datarow. 
The [Inputscreen](basics/inputscreen.md) provides Inputfields for Datamodification.

### [Customizer-System](basics/eav.md)

Some Modules appears empty: No Input Fields are displayed instead you will see a blank page.
This kind of modules are intended to be customized and filled with content by you.
Tshotel provides the ability to define custom Inputfields besided the core-defined inputfields in allmost all Programmmodules.
Head to the [Customizer-System](basics/eav.md) to learn how to do that.

### [Multitenancy Support](propertys/overview.md)

Manage multiple Clients with nesting support (Multitenancy).
Read the Documentation about the [Entity-Tree](propertys/overview.md) to learn more.

### [Propertymanagement](management/overview.md)

The Propertymanagement group all Hotel-related functions in one Module and acts as Controlpanel for Hotelmanagers.